package org.manjong.safetylogin;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.gson.JsonObject;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.crypto.Cipher;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;


public class LoginActivity extends ActionBarActivity implements View.OnClickListener {

    private EditText etId;
    private EditText etPassword;
    private Button btnLogin;
    private Button btnSignup;
    // RSA 공개키
    private String publicKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etId = (EditText) findViewById(R.id.etId);
        etPassword = (EditText) findViewById(R.id.etPassword);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnSignup = (Button) findViewById(R.id.btnSignup);

        btnLogin.setOnClickListener(this);
        btnSignup.setOnClickListener(this);

        RestRequestHelper requestHelper = RestRequestHelper.newInstance();

        // RSA 공개키를 받아오기 위한 요청
        requestHelper.login(new Callback<JsonObject>() {
            @Override
            public void success(JsonObject jsonObject, Response response) {
                publicKey = jsonObject.get("public_key").toString();
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public RSAPublicKey getPublicKeyFromString(String key)
            throws IOException, GeneralSecurityException {
        KeyFactory kf = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(
                Base64.decode(key, Base64.DEFAULT));
        RSAPublicKey pubKey = null;
        try {
            pubKey = (RSAPublicKey) kf.generatePublic(keySpec);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pubKey;
    }

    public String encrypt(String rawText, RSAPublicKey publicKey)
            throws IOException, GeneralSecurityException {
        Cipher cipher = Cipher.getInstance(
                "RSA/NONE/OAEPWithSHA1AndMGF1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return new String(Base64.encode(
                cipher.doFinal(rawText.getBytes()), Base64.DEFAULT));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                String phone = etId.getText().toString();
                String password = etPassword.getText().toString();

                try {
                    String encryptedPhone = encrypt(phone,
                            getPublicKeyFromString(publicKey));
                    String encryptedPassword = encrypt(password,
                            getPublicKeyFromString(publicKey));

                    RestRequestHelper requestHelper = RestRequestHelper.newInstance();

                    requestHelper.login(encryptedPhone, encryptedPassword,
                            new Callback<JsonObject>() {
                        @Override
                        public void success(JsonObject jsonObject, Response response) {
                            Toast.makeText(LoginActivity.this, jsonObject.get("message")
                                    .toString(), Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            error.printStackTrace();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.btnSignup:
                startActivity(new Intent(this, SignupActivity.class));
                break;
        }
    }
}
