package org.manjong.safetylogin;

import com.google.gson.JsonObject;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.*;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Created by manjong on 15. 1. 14..
 */
public class RestRequestHelper {

    private static RestRequestHelper instance;

    private RestCookieManager cookieManager;
    private RestAdapter restAdapter;
    private RestRequest restRequest;

    public static RestRequestHelper newInstance() {
        if (instance == null) {
            instance = new RestRequestHelper();
        }
        return instance;
    }

    public RestRequestHelper() {
        cookieManager = new RestCookieManager();
        CookieHandler.setDefault(cookieManager);

        restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://127.0.0.1:5000")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        // 쿠키 설정
                        request.addHeader("Cookie", cookieManager.getCurrentCookie());
                    }
                })
                .build();

        restRequest = restAdapter.create(RestRequest.class);

    }

    public interface RestRequest {

        @FormUrlEncoded
        @POST("/api/user/signup")
        void signUp(@Field("id") String id,
                    @Field("name") String name,
                    @Field("password") String password,
                    Callback<JsonObject> callback);

        @FormUrlEncoded
        @PUT("/api/user/login")
        void login(@Field("id") String phone,
                   @Field("password") String password,
                   Callback<JsonObject> callback);

        @GET("/api/user/login")
        void login(Callback<JsonObject> callback);

        @GET("/api/user/profile")
        void profile(Callback<JsonObject> callback);

    }

    public void signUp(String id, String name, String password, Callback<JsonObject> callback) {
        restRequest.signUp(id, name, password, callback);
    }

    public void login(Callback<JsonObject> callback) {
        restRequest.login(callback);
    }

    public void login(String id, String password, Callback<JsonObject> callback) {
        restRequest.login(id, password, callback);
    }

    public void profile(Callback<JsonObject> callback) {
        restRequest.profile(callback);
    }

}

class RestCookieManager extends CookieManager {

    private String currentCookie;

    @Override
    public void put(URI uri, Map<String, List<String>> stringListMap) throws IOException {
        super.put(uri, stringListMap);
        if (stringListMap != null && stringListMap.get("Set-Cookie") != null)
            for (String string : stringListMap.get("Set-Cookie")) {
                if (string.contains("session")) {
                    currentCookie = string;
                }
            }
    }

    public String getCurrentCookie() {
        return currentCookie;
    }
}
