package org.manjong.safetylogin;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.gson.JsonObject;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class SignupActivity extends ActionBarActivity implements View.OnClickListener {

    private EditText etId;
    private EditText etPassword;
    private EditText etName;

    private Button btnSignup;
    private Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        etId = (EditText) findViewById(R.id.etId);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etName = (EditText) findViewById(R.id.etName);

        btnSignup = (Button) findViewById(R.id.btnSignup);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        btnSignup.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_signup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignup:
                String id = etId.getText().toString();
                String password = etPassword.getText().toString();
                String name = etName.getText().toString();
                RestRequestHelper requestHelper =
                        RestRequestHelper.newInstance();

                requestHelper.signUp(id, name, password,
                        new Callback<JsonObject>() {
                    @Override
                    public void success(JsonObject jsonObject,
                                        Response response) {
                        String resultMessage =
                                jsonObject.get("message").toString();
                        Toast.makeText(SignupActivity.this,
                                resultMessage, Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        error.printStackTrace();
                    }
                });

                break;
            case R.id.btnCancel:
                finish();
                break;

        }
    }
}
